#
# Be sure to run `pod lib lint FTCollectionviewflowlayout.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'FTCollectionviewflowlayout'
  s.version          = '0.1.0'
  s.summary          = 'FTCollectionviewflowlayout for Collectionview'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
A short description of FTCollectionviewflowlayout.
Fork from MHCollectionviewflowlayout
                       DESC

  s.homepage         = 'https://bitbucket.org/clubfactory/ftcollectionviewflowlayout'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { '薛明浩' => 'xue_minghao@qq.com' }
  s.source           = { :git => 'https://bitbucket.org/clubfactory/ftcollectionviewflowlayout.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'FTCollectionviewflowlayout/Classes/**/*'
  
  # s.resource_bundles = {
  #   'FTCollectionviewflowlayout' => ['FTCollectionviewflowlayout/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
   s.frameworks = 'UIKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
